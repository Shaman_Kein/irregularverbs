#include "Widget.h"

#include <QTableView>
#include <QHeaderView>
#include <QPushButton>
#include <QVBoxLayout>

#include "ItemModel.h"
#include "dlgaddirregularverbs.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QVBoxLayout *box = new QVBoxLayout;
    box->setMargin(5);
    box->setSpacing(5);

    btnAdd = new QPushButton("add");
    btnAdd->setFixedSize(60, 40);
    connect(btnAdd, &QPushButton::clicked, this, &Widget::addIrregularVerbs);
    tab = new QTableView();
    ItemModel * model = new ItemModel;

//    tab->verticalHeader()->hide();

    for(int i=0;i < 50; i++) {
        model->appendRow({"быть","be","was","been"});
    }

    tab->setModel(model);


    box->addWidget(btnAdd,0, Qt::AlignRight);
    box->addWidget(tab);
    setLayout(box);
}

Widget::~Widget()
{

}

void Widget::resizeEvent(QResizeEvent *)
{
    const int w = tab->width()-45;
    tab->setColumnWidth(0, w*2/5);
    tab->setColumnWidth(1, w*3/5);
}

void Widget::addIrregularVerbs()
{
    DlgAddIrregularVerbs *dlg = new DlgAddIrregularVerbs();
    if(dlg->exec() == QDialog::Accepted) {
//        qDebug() << "ок";
    }
}
