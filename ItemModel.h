#ifndef ITEMMODEL_H
#define ITEMMODEL_H

#include <QAbstractTableModel>

#include "Item.h"

class ItemModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ItemModel(QObject *parent = nullptr);
    ~ItemModel() override;

    int rowCount(const QModelIndex &paren = QModelIndex()) const override;
    int columnCount(const QModelIndex &paren = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
//    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    void appendRow(const Item & item);

signals:

private:
    QList<Item> items;

};

#endif // ITEMMODEL_H
