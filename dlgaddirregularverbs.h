#ifndef DLGADDIRREGULARVERBS_H
#define DLGADDIRREGULARVERBS_H

#include <QDialog>


struct IrregularVerbs {
    QString str1;
    QString str2;
    QString str3;
};

class DlgAddIrregularVerbs : public QDialog
{
public:
    DlgAddIrregularVerbs();

    IrregularVerbs getIrregularVerbs();

private:
    IrregularVerbs value;
};

#endif // DLGADDIRREGULARVERBS_H
