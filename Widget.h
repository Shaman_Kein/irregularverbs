#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class QTableView;
class QPushButton;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

protected:
    void resizeEvent(QResizeEvent*);
    void addIrregularVerbs();

private:
     QTableView* tab;
     QPushButton* btnAdd;
};
#endif // WIDGET_H
