#ifndef ITEM_H
#define ITEM_H

#include <QString>

class Item
{
public:
    Item(QString ru, QString enV1, QString enV2, QString enV3);

    QString getRu() const;
    void setRu(const QString &value);

    QString getEnFirst() const;
    void setEnFirst(const QString &value);

    QString getEnSecond() const;
    void setEnSecond(const QString &value);

    QString getEnThird() const;
    void setEnThird(const QString &value);

private:
    QString ru;
    QString enFirst;
    QString enSecond;
    QString enThird;
};

#endif // ITEM_H
