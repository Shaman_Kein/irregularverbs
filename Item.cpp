#include "Item.h"

Item::Item(QString ru, QString enV1, QString enV2, QString enV3)
{
    this->ru = ru;
    enFirst = enV1;
    enSecond = enV2;
    enThird = enV3;
}

QString Item::getRu() const
{
    return ru;
}

QString Item::getEnFirst() const
{
    return enFirst;
}

void Item::setEnFirst(const QString &value)
{
    enFirst = value;
}

QString Item::getEnSecond() const
{
    return enSecond;
}

QString Item::getEnThird() const
{
    return enThird;
}

void Item::setEnThird(const QString &value)
{
    enThird = value;
}

void Item::setRu(const QString &value)
{
    ru = value;
}

void Item::setEnSecond(const QString &value)
{
    enSecond = value;
}
