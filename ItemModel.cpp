#include "ItemModel.h"

ItemModel::ItemModel(QObject *parent) : QAbstractTableModel(parent)
{

}

ItemModel::~ItemModel()
{

}

int ItemModel::rowCount(const QModelIndex &) const
{
    return items.count();
}

int ItemModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QVariant ItemModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole && role != Qt::EditRole) return {};
    const auto & item = items[index.row()];
    switch (index.column()) {
    case 0: return item.getRu();
    case 1: return QString("%1-%2-%3").arg(item.getEnFirst()).arg(item.getEnSecond()).arg(item.getEnThird());
    default: return {};
    }
}

void ItemModel::appendRow(const Item &item)
{
    beginInsertRows({}, items.count(),items.count());
    items.append(item);
    endInsertRows();
}

//QVariant ItemModel::headerData(int section, Qt::Orientation orientation, int role) const
//{
//    return QVariant();
//}

