#include "dlgaddirregularverbs.h"

#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>

DlgAddIrregularVerbs::DlgAddIrregularVerbs()
{
    resize(500,300);
    QGridLayout* grid = new QGridLayout;

    QLabel* lbl;
    QLineEdit* edit;

    for (int i=0; i < 3; i++) {
        lbl = new QLabel(QString("Lbl %1").arg(i));
        grid->addWidget(lbl, i, 0);

        edit = new QLineEdit();
        grid->addWidget(edit, i, 1);
    }


    QPushButton * btnAcept = new QPushButton("ок");
    QPushButton * btnCancel = new QPushButton("отмена");
    connect(btnAcept, &QPushButton::clicked, this, &QDialog::accept);
    connect(btnCancel, &QPushButton::clicked, this, &QDialog::reject);

    grid->addWidget(btnAcept, 3, 0);
    grid->addWidget(btnCancel, 3,1);

    setLayout(grid);


    value.str1 = "aaa";
    value.str1 = "bbb";
    value.str1 = "ccc";
}

IrregularVerbs DlgAddIrregularVerbs::getIrregularVerbs()
{
    return value;
}
